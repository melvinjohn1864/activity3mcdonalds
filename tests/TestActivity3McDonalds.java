import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestActivity3McDonalds {
	
	//COnstant parameters used in the file
	
    WebDriver driver;
	
	final String CHROME_DRIVER_LOCATION = "/Users/owner/Desktop/chromedriver";
	
	final String baseUrl = "http://www.mcdonalds.ca";

	@Before
	public void setUp() throws Exception {
		// Setting up the selenium
		
		System.setProperty("webdriver.chrome.driver",CHROME_DRIVER_LOCATION);
		driver = new ChromeDriver();
				
		// 3. Open Chrome and go to the base url;
		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		
		//Close the chrome web driver after the testing is done
		
		Thread.sleep(8000);  //pause for 2 seconds before closing the browser
		driver.close();
	}

	@Test
	public void test() {
		// Testing the first test case
		
		//TC1:  Title of the subscription feature is “Subscribe to my Mcd’s”
		
		checkFirstTestCase();
		
		
		//Testing the second test case
		
		//TC2:  Email signup - happy path
		
		checkSecondTestCase();
		
		//Testing the second test case
		
		//TC3: Email signup -  negative case
				
		checkThirdTestCase();


	}
	
	private void checkFirstTestCase() {
		//Steps
		
		//1. Go to the website
		//2. Get the actual output from screen
		//3. check if actual output = expected output
				
		WebElement subscriptionTitle = driver.findElement(By.cssSelector("h2.click-before-outline"));
				
		assertEquals("Subscribe to My McD’s®", subscriptionTitle.getText());
		
	}
	
	private void checkSecondTestCase() {
		//Steps
		
		//1. Go to the website
		//2. Enter the first name
		//3. Enter the email address
		//4. Enter the first 3 digits of a valid postal code.
		//5. Click Subscribe button
		//6. Check if actual output = expected output
				
		WebElement firstNameBox = driver.findElement(By.id("firstname2"));
		firstNameBox.sendKeys("Melvin");
				
		WebElement emailBox = driver.findElement(By.id("email2"));
		emailBox.sendKeys("melvinjohn1864@gmail.com");
				
		WebElement postalCodeBox = driver.findElement(By.id("postalcode2"));
		postalCodeBox.sendKeys("M1E");
				
		WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		subscribeButton.click();
		
	}
	
	private void checkThirdTestCase() {
		//Steps
		
		//1. Go to the website
		//2. Do nnot enter the first name
		//3. Do not enter the email address
		//4. Do not enter the first 3 digits of a valid postal code.
		//5. Click subscribe button
		//6. Check if actual output = expected output
				
				
		WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));
		subscribeButton.click();
		
	}

}
