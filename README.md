## Activity 3 - McDonalds Website

##### Group Members

* MELVIN JOHN : C0742327
* JYOTHIS RAJAN : C0744903

##### Test Cases

* TC1:  Title of the subscription feature is “Subscribe to my Mcd’s”
* TC2:  Email signup - happy path
* TC3:  Email signup - negative case
